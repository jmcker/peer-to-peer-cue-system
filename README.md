# Peer-to-Peer Cue System #

Cue system for simple two-way communication and visual cueing using WebRTC technology.
v1.0

### Setup ###

1. Open receive-cue.html on the receiving device.
2. Open send-cue.html on the sending device.
3. Both should indicate in the *Status* box that they have been connected.



Should either username indicate that it has already been taken, wait a day for the hold to expire on the server, or manually change the id in BOTH send-cue.html and receive-cue.html.


### Contact ###
* Jack McKernan
* [jmcker@outlook.com](mailto:jmcker@outlook.com)
* [jackmckernan.tk](jackmckernan.tk)